import socket
import json
BUFSIZE = 16384
def ordFunc(e):
    return e[0]

class Elemento(object):
    def __init__(self, hostIP):
        self.receiver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection = ("localhost", 8080)
        self.receiver.bind(connection)
        self.sender = []
        self.elementList = []
    
    def connect(self, knownIp):
        """Função de conexão"""
        IpList = self.getIpList(knownIp)
        connected = False
        while not connected:
            pingList = self.pingTest(IpList)
            pingList.sort(key = ordFunc)

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            for pings in pingList:
                address = (pings[0], 8080)
                try:
                    s.connect(address)
                    connected = True
                    break
                except:
                    print("Falha na conexão com " + address[0])
                    
            
    def createMessage(self, msgType, content=None):
        """
        Cria uma mensagem do tipo msgType com o conteúdo content\n
        content deve ser uma string quando presente\n
        msgType = 0: Pedido de Lista de Ips\n
        msgType = 1: Pedido de Conexão\n
        msgType = 2: PingTest\n
        msgType = 3: Mensagem Normal\n
        msgType = 4: Adicionar na Lista de Ips do Grupo\n
        msgType = 5: Remover na Lista de Ips do Grupo\n
        """
        message = bytes(str(msgType), "utf8")
        if msgType > 2:
            message = message + bytes(content, "utf8")

        return message

    def getIpList(self, knownIp):
        """Função que retorna a lista de IPs"""
        message = self.createMessage(0)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection = (knownIp, 8080)
        s.bind(connection)

        s.sendall(message)


        strIpList = s.recv(BUFSIZE).decode("utf8")
        ipList = strIpList.loads()

        return ipList

    def sendMsg(self, message):
        """Função que envia uma mensagem para os outros elementos"""
        pass

    def pingTest(self, knownIp):
        """"""
        return [(3,4)]
