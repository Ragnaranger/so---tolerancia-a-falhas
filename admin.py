from socket import *
import argparse
import pickle


# ================== TAGS DE SEPARAÇÃO ==================
# Separador de header e content da mensagem
SEPARATOR          = 'SEPSEPSEPSPESPEPSPEPSPESPEPSPEPSPE'
# Pedido para se conectar à rede
CONNECT_ME         = 'CONMEOCNECMOENCOEMCOENCOEMCEONCOME'
# Pedido pra fazer pingtest
LETS_PING_TEST     = 'GPTINGSPSITNGSPITNGTSESEGNPISENTPS'
# Requisição da lista de ip's
SEND_IP_LIST       = 'IPLSIPLSPILSPILSPISLPILSPISLISLISP'
# Recusar pedido de se conectar à rede ou teste de ping
CANT_CONNECT       = 'CATNACONATOCNAOTNCONACNAOTNCNAONTN'
# Aceitar pedido de se conectar à rede ou teste de ping
CAN_CONNECT        = 'CANCAOCNAOCNCNANOCANOCANOCNOANCAON'
# Pedido para adicionar ip à lista de conectados
ADD_THIS_IP        = 'DPAAPDIAPIDAPIDPAIDPAIDPAIDPAIDPAI'
# Uma mensagem do chat
MESSAGE            = 'SMGSGSMGMSGMSGMSMGMSGMSMGMSMGMSMGM'
SEND_CONNECTED_LIST = 'SKDFJNVSDKLJNDFKJNVKLFDSNVLDSKJFN'

HOST = input('Insira seu próprio Ip: ')
PORT = 8082
BUFFERSIZE = 1024

CONNECTED_SOCKET_LIST = []

NETWORK_LIST = []
TARGET_IP:str
TARGET_PORT:int

def recv_network_list(socket):
    global NETWORK_LIST
    stream = socket.recv(BUFFERSIZE)
    NETWORK_LIST = pickle.loads(stream)


def recv_connected_list(socket):
    """Função para receber os membros de uma rede"""
    global CONNECTED_SOCKET_LIST
    stream = socket.recv(BUFFERSIZE)
    CONNECTED_SOCKET_LIST = pickle.loads(stream)

def connect():
    global TARGET_IP, TARGET_PORT, CONNECTED_SOCKET_LIST, NETWORK_LIST, CONNECTED_ONCE

    
    sock = socket(AF_INET, SOCK_STREAM)


    print('Conectando em: ', TARGET_IP, TARGET_PORT, ' para pedir lista de Ip\'s')
    sock.connect((TARGET_IP, TARGET_PORT))

    # Pegar lista de IP's:
    sock.send(bytes(SEND_IP_LIST + SEPARATOR + '0', encoding='utf8'))
    recv_network_list(sock)
    sock.close()

def connect_socket(item):
    global TARGET_IP, TARGET_PORT, CONNECTED_SOCKET_LIST, NETWORK_LIST, CONNECTED_ONCE

    TARGET_IP = item[0]
    TARGET_PORT = item[1]
    sock = socket(AF_INET, SOCK_STREAM)


    # print('Conectando em: ', TARGET_IP, TARGET_PORT, ' para pedir lista de Ip\'s')
    sock.connect((TARGET_IP, TARGET_PORT))

    # Pegar lista de IP's:
    sock.send(bytes(SEND_CONNECTED_LIST + SEPARATOR + '0', encoding='utf8'))
    recv_connected_list(sock)
    sock.close()



def handle_args():
    global TARGET_IP
    global TARGET_PORT
    parser = argparse.ArgumentParser()
    parser.add_argument('ip',
                        action='store',
                        help='IP do host para enviar o arquivo')
                        
    parser.add_argument('port',
                        action='store',
                        help='Port do host para enviar o arquivo')

    args = parser.parse_args()
    TARGET_IP = args.ip
    TARGET_PORT = int(args.port)

if __name__ == "__main__":
    # global CONNECTED_SOCKET_LIST
    handle_args()
    connect()
    print('Todos os conectados: ')
    print(NETWORK_LIST)

    for item in NETWORK_LIST:
        connect_socket(item)
        print()
        print('Conexões de ', item, ':')
        print(CONNECTED_SOCKET_LIST)
