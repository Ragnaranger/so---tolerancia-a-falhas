"""@package docstring
Documentação do broadcast.

O projeto consiste em fazer um grupo de chat onde as mensagens são enviadas por um broadcast construído
usando comunicação peer-to-peer. A interface do projeto foi feita utilizando tkinter. Para rodar o programa,
a linha de comando rodada é a seguinte:
Para criar um novo grupo: python chat.py -1 8080

Para entrar em um grupo existente: python chat.py <ip> 8080
onde <ip> representa o ip de qualquer membro do grupo
"""

from socket import *
from threading import Thread
from time import time
import argparse
import pickle
import tkinter

# ================== TAGS DE SEPARAÇÃO ==================
# Separador de header e content da mensagem
SEPARATOR          = 'SEPSEPSEPSPESPEPSPEPSPESPEPSPEPSPE'
# Pedido para se conectar à rede
CONNECT_ME         = 'CONMEOCNECMOENCOEMCOENCOEMCEONCOME'
# Pedido pra fazer pingtest
LETS_PING_TEST     = 'GPTINGSPSITNGSPITNGTSESEGNPISENTPS'
# Requisição da lista de ip's
SEND_IP_LIST       = 'IPLSIPLSPILSPILSPISLPILSPISLISLISP'
# Requisição da lista de conectados
SEND_CONNECTED_LIST = 'SKDFJNVSDKLJNDFKJNVKLFDSNVLDSKJFN'
# Recusar pedido de se conectar à rede ou teste de ping
CANT_CONNECT       = 'CATNACONATOCNAOTNCONACNAOTNCNAONTN'
# Aceitar pedido de se conectar à rede ou teste de ping
CAN_CONNECT        = 'CANCAOCNAOCNCNANOCANOCANOCNOANCAON'
# Pedido para adicionar ip à lista de conectados
ADD_THIS_IP        = 'DPAAPDIAPIDAPIDPAIDPAIDPAIDPAIDPAI'
# Uma mensagem do chat
MESSAGE            = 'SMGSGSMGMSGMSGMSMGMSGMSMGMSMGMSMGM'

HOST = input('Digite seu próprio ip: ')#'192.168.15.4'
PORT = 8080
ADDR = (HOST, PORT)
BUFFERSIZE = 1024

TARGET_IP:str
TARGET_PORT:int

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

CONNECTED_SOCKET_LIST = []
NETWORK_LIST = []

# Guarda se essa instância do chat já se conectou ao menos uma vez
CONNECTED_ONCE = False

def send_network_list(socket):
    """Função para enviar os membros de uma rede"""
    stream = pickle.dumps(NETWORK_LIST)
    socket.send(stream)

def recv_network_list(socket):
    """Função para receber os membros de uma rede"""
    global NETWORK_LIST
    stream = socket.recv(BUFFERSIZE)
    NETWORK_LIST = pickle.loads(stream)


def send_connected_list(socket):
    """Função para enviar os membros de uma rede"""
    new_item = []
    for item in CONNECTED_SOCKET_LIST:
        new_item.append(item[1])
    stream = pickle.dumps(new_item)
    socket.send(stream)

def recv_connected_list(socket):
    """Função para receber os membros de uma rede"""
    global CONNECTED_SOCKET_LIST
    stream = socket.recv(BUFFERSIZE)
    CONNECTED_SOCKET_LIST = pickle.loads(stream)


def accept_incoming_connections():
    """Aceita novas comunicações
    
    A função fica em um loop aceitando comunicações novas e criando threads para lidar com elas
    """
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s has connected." % client_address)
        Thread(target=handle_client, args=(client,client_address,)).start()

def remove_by_socket(socket : socket):
    """Remove um usuário da lista de vizinhos utilizando um dado socket"""
    global CONNECTED_SOCKET_LIST
    for client in CONNECTED_SOCKET_LIST:
        if client[0] == socket:
            CONNECTED_SOCKET_LIST.remove(client)

def check_if_already_in(address):
    """Função para verificar se já existe uma conexão com o endereço enviado"""
    for element in CONNECTED_SOCKET_LIST:
        if element[1] == address:
            return True
    return False

def handle_client(client, client_address, handled=False): 
    """A função cuida da comunicação direta entre dois usuários vizinhos
    
    A função é responsável por enviar e receber mensagens de um usuário vizinho. Ela recebe e interpreta
    as mensagens recebidas por aquele vizinho.
    """
    global CONNECTED_SOCKET_LIST, NETWORK_LIST, CONNECTED_ONCE
    connect = True
    if not handled:
        connect = False
        msg = client.recv(BUFFERSIZE).decode('utf8')
        msg = msg.split(SEPARATOR)
        header = msg[0]
        content = msg[1]
        
        if header == CONNECT_ME:
            if len(CONNECTED_SOCKET_LIST) > 2:
                client.send(bytes(CANT_CONNECT + SEPARATOR + '0', encoding='utf8'))
                client.close()
            else:
                client.send(bytes(CAN_CONNECT + SEPARATOR + '0', encoding='utf8'))
                client_address = (client_address[0], int(content))
                # ip_port = (client_address[0], int(content))
                if not check_if_already_in(client_address):
                    CONNECTED_SOCKET_LIST.append((client, client_address))
                else:
                    client.close()
                    return
                connect = True
                CONNECTED_ONCE = True
                #print('entrando no while do handle')

        elif header == LETS_PING_TEST:
            ping_test_connected(client)
            client.close()

        elif header == SEND_IP_LIST:
            send_network_list(client)
            client.close()

        elif header == SEND_CONNECTED_LIST:
            # print('OLHA A CONECTED', CONNECTED_SOCKET_LIST)
            send_connected_list(client)
            client.close()


    while connect:
        try:
            msg = client.recv(BUFFERSIZE)
        except Exception:
            remove_by_socket(client)

            if client_address in NETWORK_LIST:
                NETWORK_LIST.remove(client_address)

            client.close()
            #print('olha as coisas quando caem: ', CONNECTED_ONCE, len(CONNECTED_SOCKET_LIST))
            if (len(CONNECTED_SOCKET_LIST) == 0) and CONNECTED_ONCE and len(NETWORK_LIST) > 1:
                RECONNECT_THREAD = Thread(target=reconnect)
                RECONNECT_THREAD.start()
            return

        broadcast(msg, client_address)
        handle_message(msg, client_address)

def handle_message(msg, client_address):
    """Função interpreta uma mensagem recebida de um broadcast
    
    A função é utilizada para executar comandos para o controle de falhas, além do envio de mensagens
    """
    global NETWORK_LIST, msg_list, tkinter
    msg = msg.decode('utf8')
    if msg == '':
        return
    print('Mensagem onde da bug', msg)
    msg = msg.split(SEPARATOR)
    header = msg[0]
    content = msg[1]

    if header == ADD_THIS_IP:
        #print("entrei no add_this_ip com content :" + content)
        content = content.split(':')
        content[1] = int(content[1])
        address = (content[0], content[1])
        if not (address in NETWORK_LIST):
            NETWORK_LIST.append(address)
            #print('Olha a network list: ', NETWORK_LIST)

    elif header == MESSAGE:
        msg_list.insert(tkinter.END, client_address[0] + ': ' + content)
        print(client_address[0], ': ', content)

def broadcast(msg, address):  # prefix is for name identification.
    """Envia a mensagem para todos os membros vizinhos"""
    if msg.decode == '':
        return
    print("Tamanho da lista de sockets: ", len(CONNECTED_SOCKET_LIST))
    print("mensagem: ", msg)
    for sock in CONNECTED_SOCKET_LIST:
        if sock[1] != address:
            try:
                sock[0].send(msg)
            except Exception:
                remove_by_socket(sock) # Remove o socket da lista de conectados
                # CONNECTED_SOCKET_LIST.remove(sock)
                sock[0].close()


def send_message():  # event is passed by binders.
    """Lida com o envio de mensagens
    
    A função lê a entrada na interface gráfica e envia a mensagem quando o botão de envio é pressionado
    """
    global my_msg, msg_list, tkinter
    msg = my_msg.get()
    my_msg.set("")
    msg_list.insert(tkinter.END, "Voce: " + msg)
    broadcast(bytes(MESSAGE + SEPARATOR + msg, encoding='utf8'), (HOST, PORT))

# Interface Gráfica

top = tkinter.Tk()
top.title("Broadcast")

messages_frame = tkinter.Frame(top)
my_msg = tkinter.StringVar()  # For the messages to be sent.
scrollbar = tkinter.Scrollbar(messages_frame)  # To navigate through past messages.
# Following will contain the messages.
msg_list = tkinter.Listbox(messages_frame, height=15, width=250, yscrollcommand=scrollbar.set)
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
msg_list.pack()
messages_frame.pack()

entry_field = tkinter.Entry(top, textvariable=my_msg)
entry_field.bind("<Return>", send_message)
entry_field.pack()
send_button = tkinter.Button(top, text="Send", command=send_message)
send_button.pack()
# top.protocol("WM_DELETE_WINDOW", on_closing)

# def on_closing():
#     pass

def get_lowest_ping():
    """Função que calcular o menor ping entre o membro novo e o grupo

    A função calcula o ping entre um membro novo e todos os membros do grupo e retorna o endereço do membro
    com o menor ping
    """
    addresses_by_lowest_ping = []
    for address in NETWORK_LIST:

        # Evitar que faça teste de ping com si mesmo
        if address == (HOST, PORT):
            continue

        print('Testando ping para: ', address)
        sock = socket(AF_INET, SOCK_STREAM)
        sock.connect(address)

        # Pedir teste de ping
        sock.send(bytes(LETS_PING_TEST + SEPARATOR + '0', encoding='utf8'))

        # Executar teste
        ping = ping_test_connector(sock)
        if ping == -1:
            continue

        if len(addresses_by_lowest_ping) == 0:
            addresses_by_lowest_ping.append((ping, address))
        else:
            for i in range(len(addresses_by_lowest_ping)):
                if addresses_by_lowest_ping[i][0] > ping:
                    addresses_by_lowest_ping = addresses_by_lowest_ping[:i] + [(ping, address)] + addresses_by_lowest_ping[i:]

    return addresses_by_lowest_ping

def ping_test_connector(sock):
    """Teste de ping.

    A função é executada pelo membro que deseja entrar na rede, e é utilizada para calcular o ping
    """
    start = time()

    for i in range(10):
        sock.recv(200)
        # try:
        # except Exception:
        #     print('Erro durante teste de ping, pulando elemento...')
        #     return -1


    end = time()
    return end - start

def ping_test_connected(sock):
    """Teste de ping.

    A função é executada pelo membro que já faz parte da rede, e é utilizada para calcular o ping
    """
    package = bytearray(56)
    for i in range(10):
        sock.send(package)

def reconnect():
    """Função para reconectar um membro do grupo em caso de falha
    
    A função faz a chamada das funções connect e handle_client novamente.
    """
    sock = connect(already_have_list=True)
    handle_client(sock, (TARGET_IP, TARGET_PORT), handled=True)

def connect(already_have_list=False):
    """Função para se conectar a um grupo
    
    A função inica a conexão do usuário em uma rede. A função consiste em pegar a lista de usuários conectados
    na rede, descobrir o menor ping entre ele e os outros membros e fazer a conexão.
    """
    global TARGET_IP, TARGET_PORT, CONNECTED_SOCKET_LIST, NETWORK_LIST, CONNECTED_ONCE

    connected = False
    sock = socket(AF_INET, SOCK_STREAM)

    if TARGET_IP != '-1':
        if not already_have_list:
            print('Conectando em: ', TARGET_IP, TARGET_PORT, ' para pedir lista de Ip\'s')
            sock.connect((TARGET_IP, TARGET_PORT))

            # Pegar lista de IP's:
            sock.send(bytes(SEND_IP_LIST + SEPARATOR + '0', encoding='utf8'))
            recv_network_list(sock)
            sock.close()

        # Fazer teste de ping
        lowest_ping_list = get_lowest_ping()
        i = 0
        while not connected and i < len(lowest_ping_list):
            TARGET_IP = lowest_ping_list[i][1][0]
            TARGET_PORT = lowest_ping_list[i][1][1]

            sock = socket(AF_INET, SOCK_STREAM)
            sock.connect((TARGET_IP, TARGET_PORT))


            sock.send(bytes(CONNECT_ME + SEPARATOR + str(PORT), encoding='utf8'))
            msg = sock.recv(BUFFERSIZE).decode('utf8')
            msg = msg.split(SEPARATOR)
            header = msg[0]
            content = msg[1]

            if header == CANT_CONNECT:
                i = i + 1
                sock.close()
                # sock = socket(AF_INET, SOCK_STREAM)
                # content = content.split(':')
                # TARGET_IP = content[0]
                # TARGET_PORT = int(content[1])

            elif header == CAN_CONNECT:
                CONNECTED_SOCKET_LIST.append((sock, (TARGET_IP, TARGET_PORT)))
                CONNECTED_ONCE = True
                connected = True
    else:
        # Só para, caso volte à função de conectar para uma reconexão, consiga entrar no if
        TARGET_IP = '-2'
        
    if not (HOST,PORT) in NETWORK_LIST:
        NETWORK_LIST.append((HOST, PORT))

    # Enviar mensagem para conectar os
    msg = ADD_THIS_IP + SEPARATOR + HOST + ':' + str(PORT)
    broadcast(bytes(msg, encoding='utf8'), (HOST, PORT))
    
    return sock


def handle_args():
    """Função que lida com os argumentos de entrada"""
    global TARGET_IP
    global TARGET_PORT
    parser = argparse.ArgumentParser()
    parser.add_argument('ip',
                        action='store',
                        help='IP do host para enviar o arquivo')
                        
    parser.add_argument('port',
                        action='store',
                        help='Port do host para enviar o arquivo')

    args = parser.parse_args()
    TARGET_IP = args.ip
    TARGET_PORT = int(args.port)

if __name__ == "__main__":
    """Main

    A função principal apenas lida com os argumentos de entrada e inicia as threads para aceitar outras conexões
    e lidar com o recebimento de mensagens. Além disso ela também inicia a thread da interface gráfica
    """
    
    handle_args()

    SERVER.listen(5)
    print("Waiting for connection...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()


    #SEND_THREAD = Thread(target=send_message)
    #SEND_THREAD.start()

    sock = connect()
    # handle_client(sock, (TARGET_IP, TARGET_PORT), handled=True)

    HANDLE_CLIENT_THREAD = Thread(target=handle_client, args=(sock, (TARGET_IP, TARGET_PORT), True))
    HANDLE_CLIENT_THREAD.start()

    tkinter.mainloop()

    ACCEPT_THREAD.join()
    HANDLE_CLIENT_THREAD.join()
    #SEND_THREAD.join()
    SERVER.close()
